# Linux Mint

[Linux Mint](https://www.linuxmint.com/) ist eine der einsteigerfreundlichsten Distribuitionen, da die beliebteste Programme Out-of-the-Box laufen (ohne Konfiguration). Linux Mint mit ihre hausgemachte **Cinnamon** Grafische Benutzeroberfläche machen aus deinen Computer/Laptop dein Lieblingswerkzeug, egal ob für Büroanwendungen, das Internet surfen, Videos und Filme ansehen, Spielen oder Grafikanwendungen, mit Linux Mint hast du die Performance und Stabilität die du brauchst, so einfach zu bedienen wie nie. Bei neuere Hardware auch mit aktuelleren (Linux-)Kernel mit der [Edge-Edition](https://www.linuxmint.com/edition.php?id=296)

## Getting started

Diese Anleitung hilft Ihnen dabei das Wissen zu erlangen um das Betriebssystem **Linux Mint** auf ihren Rechner zu installieren. Es wird auch gezeigt wie man Anwendungen/Programme installieren und deinstallieren kann.

Seien Sie sich bewusst dass bei eine Betriebssysteminstallation alle Daten auf der Festplatte gelöscht werden! Sie können die nächste Schritte auch testweise in eine VM([Virtuelle Maschine](https://de.wikipedia.org/wiki/Virtualisierung_%28Informatik%29)) ausführen.

Da Linux Mint ausschließlich aus freie Software besteht, kann es zu Einschränkungen bei der Nutzung der Hardware oder abspielen von Medien kommen. Wenn sie es bevorzugen, die vom Hardwarehersteller angebotene (unfreie) Drittanbieter-Firm- und Software, sowie Video-Codecs zu benutzen um die Einschränkungen umzugehen können sie dies [tun](#unfreie-software).

Sie benötigen für die nächsten Schritte ein leeres mindestens 2GB großes Speichermedium (z.B. USB-stick, DVD) um ein Boot-fähiger Installationsgerät zu erstellen, dafür brauchen Sie noch ein Programm wie [Rufus](https://rufus.ie), [balenaEtecher](https://www.balena.io/etcher/), [Win32 Disk Imager](https://sourceforge.net/projects/win32diskimager/) oder auf CD/DVD mit [Brasero](https://wiki.gnome.org/Apps/Brasero), der Ihnen dabei hilft den Startfähigen Installationsgerät zu erstellen.

Als nächstes benötigen Sie ein [Installations-Abbild](https://linuxmint.com/download.php), diese bringen Sie dann mithilfe des vorher installierten Programms auf dem Speichermedium. Linux Mint bietet verschiedene [GUIs](#grafische-benutzeroberflächen) (Grafische Benutzeroberflächen) an, wählen Sie eins aus, dass zu ihren Rechner passt.

Danach geht es weiter mit der [Installation](#installation).

Wenn sie weitere Hilfe/Infos benötigen, haben wir eine Liste weiterführende [Links](#links)

## Hardware Architekturen

* amd64

## Grafische Benutzeroberflächen

* [Cinnamon](https://linuxmint.com/rel_una_cinnamon_whatsnew.php)
* [MATE](https://linuxmint.com/rel_una_mate_whatsnew.php)
* [Xfce](https://linuxmint.com/rel_una_xfce_whatsnew.php)

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

### Unfreie Software

Wenn die WLAN-Karte nicht richtig funtioniert, oder ihr Browser keine Medien abspielt, könnte es daran liegen, dass die dazu nötigen Treiber oder Codecs fehlen da sie nicht **frei** sind. Diese können nachinstalliert werden:

Dazu öffnen wir einen Terminal...

## Links

[Download](https://linuxmint.com/download.php)
[Edge-Edition](https://www.linuxmint.com/edition.php?id=296)
[Linux Mint Debian Edition](https://linuxmint.com/download_lmde.php) (LMDE 4 "Debbie")

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment

Tiago Simões Tomé <tiagotome95@gmail.com>

## License
For open source projects, say how it is licensed.
